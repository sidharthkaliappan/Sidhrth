- 👋 Hi, I’m Sidharth Kaliappan. Final Year student at University of Geneva, Switzerland. 
- 👀 I’m interested in Data Analysis, Data Science, Machine Learning/AI and Cybersecurity. 
- 🌱 Currently learning Emotion modeling and Unsupervised Machine Learning techniques.  
- 📫 Email - sidharth.kaliappan@gmail.com

<!---
Sidhrth/Sidhrth is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
